var express=require('express');
var low=require('lowdb');
var path=require('path');
var router=express.Router();

var db=low(path.join(__dirname,'./db.json'));
router.get('/book/:isbn',(req,res)=>{
	res.json(db.get('books').find({ isbn: req.params.isbn }).value());
});
router.get('/books',(req,res)=>{
	res.json(db.get('books').value());
});
router.get('/books/:category',(req,res)=>{
	res.json(db.get('books').filter({ category: req.params.category.toLowerCase() }).value());
});
router.put('/books',(req,res)=>{
	var json={ result: true };
	try{
		if(!req.session.login)throw '沒有登入';
		var book=db.get('books').find({ isbn: req.body.isbn }).value();
		if(book)db.get('books').find({ isbn: req.body.isbn }).assign(new Book(req.body.title,req.body.isbn,req.body.author,req.body.category.toLowerCase(),book.rent)).write();
		else db.get('books').push(new Book(req.body.title,req.body.isbn,req.body.author,req.body.category.toLowerCase())).write();
	}
	catch(e){
		json.result=false;
		console.error(e);
	}
	res.json(json);
});
router.get('/books/rent/:isbn',(req,res)=>{
	var json={ result: true };
	try{
		var book=db.get('books').find({ isbn: req.params.isbn }).value();
		if(book.rent){
			book.rent=false;
			json.msg=db.get('config.checkin').value();
		}
		else{
			if(!req.query.id)throw '沒有借閱者，借閱失敗';
			book.rent=req.query.id;
			json.msg=db.get('config.checkout').value();
		}
		db.get('books').find({ isbn: req.params.isbn }).assign(book).write();
	}
	catch(e){
		json.result=false;
		console.error(e);
	}
	res.json(json);
});
router.delete('/books/:isbn',(req,res)=>{
	var json={ result: true };
	try{
		if(!req.session.login)throw '沒有登入';
		db.get('books').remove({ isbn: req.params.isbn }).write();
	}
	catch(e){
		json.result=false;
		console.error(e);
	}
	res.json(json);
});
router.get('/login',(req,res)=>{
	res.json({
		result: req.session.login=(req.session.login?true:false)
	});
});
router.post('/login',(req,res)=>{
	var admin=db.get('config.admin').value();
	if(req.body.acc===admin.acc&&req.body.pwd===admin.pwd){
		console.log('admin logined');
		req.session.login=true;
		res.json({
			result: true
		});
	}
	else res.json({
		result: req.session.login=(req.session.login?true:false)
	});
});
router.delete('/login',(req,res)=>{
	req.session.login=false;
	res.json({
		result: true
	});
})
module.exports=router;

class Book{
	constructor(title,isbn,author,category,rent){
		this.title=title;
		this.isbn=isbn;
		this.author=author;
		this.category=category||db.get('config.defaultCategory').value();
		this.rent=rent||false;
	}
}