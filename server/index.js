var express=require('express');
var low=require('lowdb');
var path=require('path');
var app=express();
const main=path.join(__dirname,'../');

var db=low(path.join(__dirname,'./db.json'));
db.defaults({
	config: {
		defaultCategory: '其他',
		notFound: '<h1>404 Not Found</h1>',
		checkin: '成功還書',
		checkout: '成功借取',
		admin: {
			acc: 'admin',
			pwd: 'admin'
		}
	},
	books: []
}).write();

app.disable('x-powered-by');
app.use(require('express-session')({
	secret: Math.random().toString(36).substring(7),
	resave: true,
	saveUninitialized: true,
	cookie: { secure: false, maxAge: 1000*60*60 }
}));
app.use(require('body-parser').json());
app.use((req,res,next)=>{
	if(!req.originalUrl.startsWith('/static'))
		console.log(`${req.method} ${req.originalUrl}`);
	next();
});

app.use('/api',require('./api.js'));
app.use('/static',express.static(path.join(main,'dist/static')));
app.get('/',(req,res)=>res.sendFile(path.join(main,'dist/index.html')));
app.all('*',(req,res)=>{
	res.status(404);
	res.send(db.get('config.notFound').value());
});

app.listen(process.argv[2]||process.env.PORT||80);