import Vue from 'vue';
import App from './App';
import router from './router';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

Vue.config.productionTip=false;

var app=new Vue({
	el: '#app',
	router,
	template: '<App/>',
	components: {
		App
	},
	data(){
		return {
			login: false
		}
	},
	created(){
		axios.get('/api/login').then(d=>{
			this.login=d.data.result;
		});
	}
});

import axios from 'axios';
window.axios=axios;