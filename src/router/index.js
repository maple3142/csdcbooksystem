import Vue from 'vue';
import Router from 'vue-router';
import books from '@/components/books';
import newbook from '@/components/new';
import modifybook from '@/components/modify';
import login from '@/components/login';
import logout from '@/components/logout';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      component: books
    },
    {
      path: '/category/',
      component: books
    },
    {
      path: '/category/:c',
      component: books
    },
    {
      path: '/new',
      component: newbook
    },
    {
      path: '/modify/:isbn',
      component: modifybook
    },
    {
      path: '/login',
      component: login
    },
    {
      path: '/logout',
      component: logout
    }
  ]
})
